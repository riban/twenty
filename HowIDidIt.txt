I created a picture with twenty different shapes each of a different colour, overlaying twenty lines of ZX BASIC code which each rhymes. The code offers to play three segments of "Crazy Words, Crazy Tune" (the Ubuntu Podcast theme music) which are each (about) twenty seconds long. This was all just for fun and to enter a competition described here: http://ubuntupodcast.org/2017/08/03/s10e22-dazzling-fancy-beast. This is a description of how I did it and what tools I used. I tried to use open source tools where possible but all are free. Most was done on a Windows PC because that is what I had to hand. (If I were to win the competition I would be able to do the same on a GNU/Linux based Entroware Apollo Laptop!)

Procedure
=========
Transcribed a recording of "Crazy Words, Crazy Tune" using MuseScore. (MuseScore file available in gitlab.)
Exported from MuseScore as a MIDI file.
Used MIDICVS to convert MIDI to csv file.
Opened csv in LibreOffice and added formula to convert to table of note,duration pairs. (ODS file available in gitlab.)
Wrote ZX Basic program within ZX-Editor, copy and pasting segments of note,duration data. (Started writing directly in FUSE but could not paste the long data strings in, so used ZX-Editor.) Saved as plain text BASIC for version control. (BASIC file available in gitlab.) Also saved tape file.
Tested with FUSE ZX Spectrum emulator.
Used Tape2WAV to create an audio file for testing on real ZX Spectrum.
Downloaded Ubuntu logo SVG and imported into GIMP as paths.
Split paths to form twenty paths. Added text (copied from BASIC program) to each path, one code line per path (text to path). Needed to tweak the font sizes to make it all fit but this isn't science - it is art!
Added a transparent layer and added twenty different shapes, each with a different colour. (GIMP file available in gitlab.)
Exported as PNG and published to 

Applications used
=================
MuseScore (https://musescore.org/en) to transcribe the music and export as MIDI file.
MIDICSV (http://www.fourmilab.ch/webtools/midicsv/) to convert MIDI file to csv.
LibreOffice (https://www.libreoffice.org/) to manipulate the csv to provide note,duration list.
ZX-Editor (http://zx-modules.de/zxeditor/zxeditor.html) to write the BASIC code because it allows pasting text, particularly the long data strings.
FUSE (http://fuse-emulator.sourceforge.net/) ZX Spectrum emulator to test the code.
Tape2WAV (http://www.worldofspectrum.org/utilities.html) to convert program to audio file.	
GIMP (https://www.gimp.org/) to draw the picture.

Credits
=======
Brian Walton (brian@riban.co.uk) for initial inspiration, coding, technical implementation and coordination.
Thanks go to Joshua Walton (joshuawaltonm@gmail.com) who transcribed the audio, and performed much of the GIMP manipulation.
Lucy Walton (riban.liw@gmail.com) who provided initial sketches (which were subsequently not used but may be submitted to same competition as separate entry).